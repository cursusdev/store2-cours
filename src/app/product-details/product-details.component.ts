import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// import { products } from '../products';
import { CartService } from '../cart.service';
import { ProductService, ProductsDetail } from '../product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  product;

  constructor(
    private route: ActivatedRoute,
    private cartService: CartService,
    private prodService: ProductService
  ) {

  }

  ngOnInit() {
    // this.route.paramMap.subscribe(params => {
    //   this.product = products[+params.get('productId')];
    // });
     this.route.paramMap.subscribe(params => {
     const productId = +params.get('productId');
      this.prodService.getProductBuId(productId).then(detail => {
        this.product = detail;
        //console.log(detail);
      });
    });
  }

  addToCart(product) {
    window.alert('Your product has been added to the cart!');
    this.cartService.addToCart(product);
  }

}