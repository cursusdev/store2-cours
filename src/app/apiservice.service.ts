import { Injectable } from '@angular/core';

export interface wpobjectmeta {
  _price:number;
}
export interface wpobjectRendered{
  rendered: string;
}
export interface wpobject{
  id: number;
  meta:wpobjectmeta;
  title:wpobjectRendered;
  excerpt:wpobjectRendered;
  content: wpobjectRendered;
} 
@Injectable({
  providedIn: 'root',
})
export class APIService {

  
  /*@TODO: APIService instances BASE_URL should be configurable */
  protected BASE_URL: string = "http://localhost:8081/wordpress/wp-json/wp/v2/";

  public list(collection: string):Promise<wpobject[]> {
    return this.fetch(collection);
  }

  public get(collection: string, id: number):Promise<wpobject> {
    return this.fetch(`${collection}/${id}`);
  }

  public post(collection: string, data: any) {
    return this.fetch(`${collection}/posts/?${data}`);
  }

  /*@TODO: Typings for APIService instances fetch method options argument */
  protected fetch(path: string, options: any = {}){ 
    /*@NOTE: Should we secure this concatenation to avoid doubles '/' ? */
    return fetch(this.BASE_URL + path, {
      "Content-type": "application/json",
      ...options
    })
    .then(res => res.json())
  }

}