import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items = [];

  constructor(
    private http: HttpClient
  ) {
    // On charge this.items depuis localStorage
      this.items = JSON.parse(localStorage.getItem('products')) || [];
      // console.log(this.items);
  }
 
  addToCart(product) {
    this.items.push(product);
    // Sauvegarder this.items dans localStorage
    localStorage.setItem('products', JSON.stringify(this.items));
  }
 
  getItems() {
    return this.items;
  }
 
  clearCart() {
    this.items = [];
    return this.items;
  }

  getShippingPrices() {
    return this.http.get('/assets/shipping.json');
  }
  
}