import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { APIService } from './apiservice.service';

export interface ProductsDetail {
  id: string;
  tilte: { rendered: string; }
  content: { rendered: string; }
  meta: {
    _price: number;
    _stock_status: boolean;
  }
}

@Injectable({
  providedIn: 'root'
})
export class WordpressService {

  // constructor(private http: HttpClient, private apiservice: APIService) { 
  //  this.getProducts();
  // }

  //  getProducts() {
  //    let productsDetail = this.apiservice.list('product');
  //    console.log(productsDetail);
  //    return productsDetail;
  //  }


  constructor(private http: HttpClient) { 
  }

  getPosts() {
    return this.http.get('http://localhost:8081/wordpress/wp-json/wp/v2/product');
  }


 
}
