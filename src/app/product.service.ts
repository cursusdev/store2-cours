import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { APIService, wpobject } from './apiservice.service';


// const products = [
//   {
//     id: 1,
//     name: 'Phone XL',
//     price: 799,
//     description: 'A large phone with one of the best screens'
//   },
//   {
//     id: 2,
//     name: 'Phone Mini',
//     price: 699,
//     description: 'A great phone with one of the best cameras'
//   },
//   {
//     id: 3,
//     name: 'Phone Standard',
//     price: 299,
//     description: ''
//   }
// ];

// export interface ProductsDetail {
//   id: string;
//   name: string;
//   price: number;
//   description: string;
// }

export interface ProductsDetail {
  id?: number,
  name: string, 
  price:number,
  description:string,
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  // products = products;
  // productsDetail: ProductsDetail[];
  // productDetail: ProductsDetail;
  // productsDetail: Observable<any[]>;
  product;

  constructor(private http: HttpClient, private apiservice: APIService) { 
    // this.getProducts();
  }

  getList():Promise<ProductsDetail[]>{
    return this.apiservice.list("product")
       .then((wpProducts:wpobject[])=>{
         return this.transformProducts(wpProducts);
       })
  }
  getProductBuId(id:number){
    return this.apiservice.get("product",id)
      .then((wpProduct:wpobject)=>{
        return this.transformProduct(wpProduct);
      })
  }
  transformProducts(wpProducts:wpobject[]): ProductsDetail[] {
    return wpProducts.map((product:wpobject)=>this.transformProduct(product))
  }

  transformProduct(wpProduct:wpobject): ProductsDetail{
    return {
      id: wpProduct.id,
      name:wpProduct.title.rendered,
      description:wpProduct.excerpt.rendered,
      price:wpProduct.meta._price,
    }
  }

  // getProd(): Promise<ProductsDetail[]> {
  //   return this.apiservice.list("product");
  // }

  // getProdById(id: number): Promise<ProductsDetail> {
    // return this.apiservice.get("product", id);
  // }

  getPost(data: any): Promise<ProductsDetail>{
    return this.apiservice.post('product', data);
  }

  // getProducts() {
  //   let productsDetail = this.apiservice.list('product');
  //   console.log(productsDetail);
  //   return productsDetail;
  // }

  // getPosts() {
  //   return this.http.get('http://localhost:8081/wordpress/wp-json/wp/v2/product');
  // }


  // getProductsDetail() {
  //   const porductsObs = from(products);
  //   return porductsObs.subscribe( x => console.log(x));
  // }

  // getProductsById(id: number): Observable<ProductsDetail> {
    // return this.getProductsDetail()
    //   .map(data => {
    //     data.map(d => d.id = d.id + "Hey")
    //    });
  // }




}
