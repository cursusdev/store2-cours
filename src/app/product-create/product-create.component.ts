import { Component, OnInit } from '@angular/core';

import { ProductService, ProductsDetail } from '../product.service';

const dataSet = [
  { 
    title: 'PC Portable IdeaPad S340-15API 81NC002VFR 15.6\" AMD Ryzen 5 8 Go RAM 1 To SATA 128 Go SSD',
    content:  'Processeur : AMD Ryzen 5 3500U de 2.1 GHz à 3.7 GHz, Mémoire : 8 Go RAM, Stockage : 1 To SATA + 128 Go SSD',
    meta: {
      price: 1500
    }  
  }
];


// let data =  'title=' + this.dataSet.title + '&content=' + this.dataSet.content;

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  
  constructor(private prodService: ProductService) {
    
    // this.addPostProduct(data);
    
  }
  
  ngOnInit() {
  }
  
  addPostProduct(data) {
    // this.prodService.getPost(data);
  }


}
