import { Component, OnInit } from '@angular/core';

// import { products } from '../products';
// import { APIService } from '../apiservice.service';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { getSyntheticPropertyName } from '@angular/compiler/src/render3/util';
import { getLocaleDayPeriods } from '@angular/common';
import { ProductService, ProductsDetail } from '../product.service';
// import { WordpressService } from '../wordpress.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent {
  // products = products;
  // products: Object;

  public products: ProductsDetail[];

  constructor(private prodService: ProductService) {
    // this.catalogService.getWProducts();
    this.prodService.getList().then(arr => { this.products = arr; });
  }
  

  // }
    // this.getData();
    // this.wpService.getPosts().subscribe(
    //   products => this.products = products
    // );
    //   console.log(this.wpService.getPosts()); // check by logging in the console
  
    /*this.products = this.apiService.list('product');*/


  share() {
    window.alert('The product has been shared!');
  }

  onNotify() {
    window.alert('You will be notified when the product goes on sale');
  }

  

  // getData() {
  //   let products = this.productService.getProducts();
  //   console.log(products);
  // }


}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/