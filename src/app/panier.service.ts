import { Injectable } from '@angular/core';
// import { LocalStorage } from '@ngx-pwa/local-storage';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Product {
  id: string;
  name: string;
  price: number;
  description: string;
}

@Injectable({
  providedIn: 'root',
})
export class PanierService {
  // productsCollection: AngularFirestoreCollection<Product>;      // Reference to users list, Its an Observable
  // cartProducts: Observable<Product[]>;
  // itemsAsync;
  // items = [];

  // constructor(private db: AngularFirestore) {
  //   this.productsCollection = db.collection<Product>('products');
  //   this.cartProducts = this.productsCollection.valueChanges();
  //   const myObservable = this.cartProducts;// myLog.valueChanges();
  //   this.itemsAsync = myObservable;
  //   myObservable.subscribe(newValue => {
  //     console.log(newValue);
  //     this.items = newValue;
  //     this.getItemsFBase();
  //   });
  // }

  // // Lecture de la liste des cartProducts
  // // getCartProduct() {
  // //   this.productsCollection = this.db.collection('products');
  // //   return this.productsCollection;
  // // }
  
  // getItemsFBase() {
  //   return this.items;
  // }

  // addItemsFBase(items) {

  // }


}

